﻿/*
 * [2019] - [2021] Eblocks Software (Pty) Ltd, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Eblocks
 * Software (Pty) Ltd.
 * and its suppliers (if any). The intellectual and technical concepts contained herein
 * are proprietary
 * to Eblocks Software (Pty) Ltd. and its suppliers (if any) and may be covered by South 
 * African, U.S.
 * and Foreign patents, patents in process, and are protected by trade secret and / or 
 * copyright law.
 * Dissemination of this information or reproduction of this material is forbidden unless
 * prior written
 * permission is obtained from Eblocks Software (Pty) Ltd.
*/

using System;

namespace PersonalBank
{
    /// <summary>
    ///     This class wil be used to manage a savings account.
    /// </summary>
    public class SavingsAccount
    {
        /// <summary>
        ///     Gets or sets a <see crf="double">double</see> that represents the Current Balance
        /// </summary>        
        public double CurrentBalance { get; set; }


        /// <summary>
        ///     This method will deposit an amount into the savings account and return the new balance.
        /// </summary>
        /// <param name="amountToDeposit"> A <see crf="double">double </see> that represents the amount to deposit.</param>
        /// <returns> A <see crf="double">double</see> which represents the new balance.</returns>
        public double DepositToSavingsAccount(double amountToDeposit)
        {
            if(amountToDeposit <= 0)
            {
                throw new ArgumentException("Cannot Desposit amount less than 1");
            }

            CurrentBalance = CurrentBalance + amountToDeposit;

            return CurrentBalance;
        }

        /// <summary>
        /// This method will withdraw an amount from the savings account and return a new balance.
        /// </summary>
        /// <param name="amountToWithdraw">A <see crf="double">double</see> that represents the amount to withdraw.</param>
        /// <returns> A <see crf ="double">double</see> which represents the new balance.</returns>
        public double WithdrawFromSavingsAccount(double amountToWithdraw)
        {
            if(amountToWithdraw <= 0)
            {
                throw new ArgumentException("Cannot withdraw a zero or negative amount");
            }

            if(CurrentBalance >= amountToWithdraw)
            {
                CurrentBalance=CurrentBalance-amountToWithdraw;
                return CurrentBalance;
            }
            else
            {
                throw new Exception("this withdrawal is exceeding balance!");
            }
        }

        /// <summary>
        ///     This method will transfer funds from this savings account to another account.
        /// </summary>
        /// <param name="toAccount">A <see crf="SavingsAccount">SavingsAccount</see> to which funds will be transfered.</param>
        /// <param name="amountToTransfer"> A <see crf="double">double </see> representing amount to be transfered.</param>
        /// <returns> A <see crf="TransferFundsReturnType">TransferFundsReturnType </see> that details the transaction results.</returns>
        public TransferFundsReturnType TransferFunds(SavingsAccount toAccount, double transferAmount)
        {
            if(transferAmount <= 0)
            {
                throw new ArgumentException("Cannot transfer 0 or negative amount");
            }

            if(toAccount == null)
            {
                throw new ArgumentNullException("Account to receive funds is null");
            }

            if(CurrentBalance < transferAmount)            
            {
                throw new Exception("Insufficient funds to transfer");
            }
            else
            {
                CurrentBalance = CurrentBalance - transferAmount;
                toAccount.CurrentBalance = toAccount.CurrentBalance + transferAmount;

                TransferFundsReturnType result =  new TransferFundsReturnType();
                result.Message = "Transfer Successful";
                result.ToAccountCurrentBalance = toAccount.CurrentBalance;
                result.FromAccountCurrentBalance = CurrentBalance;

                return result;
            }
        
        }
    }

    /// <summary>
    /// This class will be used to Return Information about our Transfer Funds Tranaction.
    /// </summary>
    public class TransferFundsReturnType
    {
        /// <summary>
        ///     Gets or sets a <see crf="string">string</see> that represents the a response message.
        /// </summary>        
        public string Message { get; set; }

        /// <summary>
        ///     Gets or sets a <see crf="double ">double</see> that represents the current balance of the Account that funds was transfered to.
        /// </summary>        
        public double ToAccountCurrentBalance { get; set; }

        /// <summary>
        ///     Gets or sets a <see crf="double ">double</see> that represents the current balance of the Account that funds was transfered from.
        /// </summary>        
        public double FromAccountCurrentBalance { get; set; }
    }
}
