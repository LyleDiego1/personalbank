using System;

namespace PersonalBank 
{
    public class Program
    {
        public static void Main()
        {
            SavingsAccount myAccount = new SavingsAccount();
            SavingsAccount myAccount2 = new SavingsAccount();
            myAccount.DepositToSavingsAccount(7500);
            Console.WriteLine(myAccount.CurrentBalance);
            myAccount.DepositToSavingsAccount(2500);
            Console.WriteLine(myAccount.CurrentBalance);
            myAccount.TransferFunds(myAccount2, 250.00);
            Console.WriteLine("My account2 current balance is {0}", myAccount2.CurrentBalance);
            Console.WriteLine("My account current balance is {0}",myAccount.CurrentBalance );
            myAccount.WithdrawFromSavingsAccount(1000);
            Console.WriteLine("after withdrawal, myaccount has {0}", myAccount.CurrentBalance);
        }
    }
}