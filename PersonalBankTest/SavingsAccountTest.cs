/*
 * [2019] - [2021] Eblocks Software (Pty) Ltd, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Eblocks
 * Software (Pty) Ltd.
 * and its suppliers (if any). The intellectual and technical concepts contained herein
 * are proprietary
 * to Eblocks Software (Pty) Ltd. and its suppliers (if any) and may be covered by South 
 * African, U.S.
 * and Foreign patents, patents in process, and are protected by trade secret and / or 
 * copyright law.
 * Dissemination of this information or reproduction of this material is forbidden unless
 * prior written
 * permission is obtained from Eblocks Software (Pty) Ltd.
*/

using System;
using Xunit;
using PersonalBank;

namespace PersonalBankTest
{
    /// <summary>
    /// This class will test SavingsAccount.
    /// </summary>
    public class SavingsAccountTest
    {
        /// <summary>
        ///     This method will test deposting an amount into savings account and test the correct balance is returned.
        /// </summary>
        [Fact]
        public void Should_Deposit_To_Savings_Account_Test()
        {
            // Arrange
            SavingsAccount account = new SavingsAccount();
            double amountToDeposit = 5000.00;

            // Act
            double result = account.DepositToSavingsAccount(amountToDeposit);

            // Assert
            Assert.Equal(result, account.CurrentBalance);
        }

        /// <summary>
        ///     This method will test if exception is thrown when deposit amount is zero or less.
        /// </summary>
        [Fact]
        public void Should_Throw_Exception_When_Deposit_Amount_IS_Zero_OR_Less_Test()
        {
            // Arrange
            SavingsAccount account = new SavingsAccount();
            double amountToDeposit = -100.00;

            // Act
             

            // Assert
            Assert.Throws<ArgumentException>(() => account.DepositToSavingsAccount(amountToDeposit));
        }

        /// <summary>
        ///     This will test withdrawing an amount from the saving account and will test if the new balance is correct.
        /// </summary>
        [Fact]
        public void Should_Withdraw_From_Savings_Account_Test()
        {
            //  Arrange
            SavingsAccount account = new SavingsAccount();
            account.CurrentBalance = 10000.00;
            double amountToWithdraw = 6000.00;

            //  Act
            double remainingBalance = account.WithdrawFromSavingsAccount(amountToWithdraw);

            //  Assert
            Assert.Equal(remainingBalance, 4000.00);
        }

        /// <summary>
        /// This method will test if exception is thrown when withdrawal amount is more than current balance.
        /// </summary>
        [Fact]
        public void Should_Throw_Exception_When_Withdrawal_Amount_IS_More_Than_Current_Balance_Test()
        {
            //  Arrange
            SavingsAccount account = new SavingsAccount();
            account.CurrentBalance = 8000.00;
            double amountToWithdraw = 12000.00;

            //  Act

            //  Assert
            Assert.Throws<Exception>(() => account.WithdrawFromSavingsAccount(amountToWithdraw));
        }

        /// <summary>
        /// This method will test if exception is thrown when withdrawal amount is 0 or negative.
        /// </summary>
        [Fact]
        public void Should_Throw_Exception_When_Withdrawal_Amount_Is_Zero_Or_Negative_Test()
        {
            //  Arrange
            SavingsAccount account = new SavingsAccount();
            double amountToWithdraw = -50.00;

            //  Act

            //  Assert
            Assert.Throws<ArgumentException>(() => account.WithdrawFromSavingsAccount(amountToWithdraw));
        }

        /// <summary>
        /// This method will test if exception is thrown when transfer amount is 0 or negative.
        /// </summary>
        [Fact]
        public void Should_Throw_Exception_When_Transfer_Amount_Is_Zero_Or_Negative()
        {
            //  Arrange
            SavingsAccount fromAccount = new SavingsAccount();
            SavingsAccount toAccount = new SavingsAccount();
            double transferAmount = -100.00;

            //  Act
            
            //  Assert
            Assert.Throws<ArgumentException>(() => fromAccount.TransferFunds(toAccount, transferAmount));
        }

        /// <summary>
        ///     This method will test if exception is thrown when AccountTo is null.
        /// </summary>
        [Fact]
        public void Should_Throw_Exception_When_AccountTo_Is_Null_Test()
        {
            //  Arrange
            SavingsAccount fromAcoount = new SavingsAccount();
            SavingsAccount toAccount = null;
            double transferAmount = 500.00;

            //  Act

            //  Assert
            Assert.Throws<ArgumentNullException>(() => fromAcoount.TransferFunds(toAccount, transferAmount));
        }

        /// <summary>
        /// This method will test if exception is thrown when FromAccount CurrentBalance is less than TransferAmount
        /// </summary>
        [Fact]
        public void Should_Throw_Exception_When_Transfer_Amount_Is_More_Than_Current_Balance_Test()
        {
            //  Arrange
            SavingsAccount fromAccount = new SavingsAccount();
            fromAccount.CurrentBalance = 300.00;
            SavingsAccount toAccount = new SavingsAccount();
            double transferAmount = 500.00;

            //  Act

            //  Assert                
            Assert.Throws<Exception>(() => fromAccount.TransferFunds(toAccount, transferAmount));
        }

        /// <summary>
        /// This method will test the CurrentBalance of FromAccount and ToAccount after a succesfull transfer.
        /// </summary>
        [Fact]
        public void Should_Test_Current_Balance_Of_From_Account_And_To_Account_Test()
        {
            // Arrange 
            SavingsAccount fromAccount = new SavingsAccount();
            fromAccount.CurrentBalance = 10000.00;
            SavingsAccount toAccount = new SavingsAccount();
            toAccount.CurrentBalance = 10000.00;
            double transferAmount = 3000.00;

            // Act 
            TransferFundsReturnType result = fromAccount.TransferFunds(toAccount, transferAmount);
            
            // Assert
            Assert.Equal(result.Message, "Transfer Successful");
            Assert.Equal(result.FromAccountCurrentBalance, 7000.00);
            Assert.Equal(result.ToAccountCurrentBalance, 13000.00);
        }
    }
}
